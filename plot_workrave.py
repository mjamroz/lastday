#!/usr/bin/env python
from datetime import datetime
from matplotlib import gridspec
import matplotlib.pyplot as plt
plt.style.use('ggplot')
from matplotlib.dates import MonthLocator, YearLocator, DateFormatter
# m 6 13636 2013320 1453420 2798 5357 20087
# mouse_usage_min  click     keystrokes
# D 10 6 118 8 31 10 6 118 16 0
#   day month0   ****          ****

fig =  plt.figure(figsize=(12,6))
fig.suptitle('Michal and his mechanical keyboard')
gs = gridspec.GridSpec(2, 2, width_ratios=[3, 1])

xaxis = []
yaxis_clicks = []
yaxis_strokes = []

with open("/home/mijam/.workrave/historystats", "r") as f:
    day = ""
    clicks = -1
    strokes = -1
    for line in f.readlines():
        d = line.split()
        if line.startswith("D"):
            year = int(d[3]) + 1900
            month = int(d[2]) + 1
            day = int(d[1])
        elif line.startswith("m"):
            if year < 2017:
                continue
            clicks = int(d[6])
            strokes = int(d[7])
            dday = datetime(year, month, day)
            xaxis.append(dday)
            yaxis_clicks.append(clicks)
            yaxis_strokes.append(strokes)

ax = plt.subplot(gs[0])
ax1 = plt.subplot(gs[2])
ax.plot_date(xaxis, yaxis_clicks, '.')
ax1.plot_date(xaxis, yaxis_strokes, '.')
ax1.set_title('Number of keyboard strokes')
ax.set_title('Number of mouse clicks')
ax.xaxis.set_major_locator(MonthLocator())
ax.xaxis.set_major_formatter(DateFormatter('%Y %m'))
ax.autoscale_view()
fig.autofmt_xdate()

hx = plt.subplot(gs[1])
hx1 = plt.subplot(gs[3])
n, bins, patches = hx.hist(filter(lambda x: x != 0, yaxis_clicks), 20)
n, bins, patches = hx1.hist(filter(lambda x: x != 0, yaxis_strokes), 20)

plt.show()
